function TemperatureConverterCtrl($scope) {
  $scope.edited = null;
  $scope.markEdited = function(which) {
    $scope.edited = which;
  };
  $scope.$watch('fahrenheit', function(value) {
    if($scope.edited == 'F') {
      $scope.celsius = (value - 32) * 5.0/9.0;
      $scope.kelvin = $scope.celsius + 273;
    }
  });
  $scope.$watch('celsius', function(value) {
    if($scope.edited == 'C') {
      $scope.fahrenheit = value * 9.0 / 5.0 + 32;
      $scope.kelvin = value + 273;
    }
  });
  $scope.$watch('kelvin', function(value) {
  	if($scope.edited == 'K'){
  		$scope.celsius = value - 273;
  		$scope.fahrenheit = $scope.celsius * 9.0 / 5.0 + 32;
  	}
  });
}